import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    '.molecule/ansible_inventory').get_hosts('all')


def test_xinted_enabled_and_running(host):
    service = host.service('xinetd')
    assert service.is_enabled
    assert service.is_running


def test_tftp_server(host):
    cmd = host.command('tftp 127.0.0.1 -c get boot/grub.cfg')
    assert cmd.rc == 0


def test_menuc32_in_proper_directory(host):
    assert host.file('/export/boot/pxelinux.cfg/menu.c32').exists
