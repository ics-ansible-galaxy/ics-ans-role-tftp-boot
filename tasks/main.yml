---
- name: install requirements
  yum:
    name: "{{ item }}"
    state: present
  with_items:
    - xinetd
    - tftp
    - tftp-server
    - grub2-efi
    - grub2-efi-modules
    - shim
    - syslinux

- name: create required folders
  file:
    path: "{{ item }}"
    state: directory
    owner: root
    group: root
    mode: 0755
  with_items:
    - /export/boot/EFI/centos/x86_64-efi
    - /export/boot/pxelinux.cfg

- name: copy TFTP configuration file
  copy:
    src: tftp
    dest: /etc/xinetd.d/tftp
    backup: yes
    owner: root
    group: root
    mode: 0600
  notify: restart xinetd

- name: add tftp to the firewall
  firewalld:
    service: tftp
    permanent: true
    immediate: yes
    state: enabled

- name: ensure xinetd is enabled and started
  systemd:
    name: xinetd
    enabled: yes
    state: started

- name: copy efi files for UEFI network boot
  copy:
    src: "/boot/efi/EFI/centos/{{ item }}"
    dest: /export/boot
    remote_src: true
    owner: root
    group: root
  with_items:
    - grubx64.efi
    - shim.efi

- name: copy *.mod and *.lst files for UEFI network boot
  shell: >
    cp /usr/lib/grub/x86_64-efi/*.{mod,lst} /export/boot/EFI/centos/x86_64-efi
  args:
    creates: /export/boot/EFI/centos/x86_64-efi/command.lst

- name: create grub configuration
  template:
    src: grub.cfg.j2
    dest: /export/boot/grub.cfg
    owner: root
    group: root
    mode: 0644

- name: copy pxelinux.0 for PXE boot
  copy:
    src: "/usr/share/syslinux/{{ item }}"
    dest: /export/boot
    remote_src: true
    owner: root
    group: root
    mode: 0644
  with_items:
    - pxelinux.0

- name: copy menu.c32 for PXE boot
  copy:
    src: "/usr/share/syslinux/{{ item }}"
    dest: /export/boot/pxelinux.cfg
    remote_src: true
    owner: root
    group: root
    mode: 0644
  with_items:
    - menu.c32

- name: create pxelinux default configuration
  template:
    src: pxelinux.cfg.j2
    dest: /export/boot/pxelinux.cfg/default
    owner: root
    group: root
    mode: 0644

- name: create ifc1210 environment script
  template:
    src: env.script.j2
    dest: /export/boot/env.script
    owner: root
    group: root
    mode: 0644
  notify: compile the ifc1210 environment script into an u-boot image

- name: copy environment script Makefile
  copy:
    src: Makefile
    dest: /export/boot/Makefile
    owner: root
    group: root
    mode: 0644
  notify: compile the ifc1210 environment script into an u-boot image
